/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restapi.app.ws.exceptions;

/**
 *
 * @author robertmarinescu
 */
public class MissingRequiredFieldException extends RuntimeException {
    private static final long serialVersionUID = -2548198640786219459L;

    public MissingRequiredFieldException(String message){
        super(message);
    }

}
