/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restapi.app.ws.ui.entrypoints;

import com.restapi.app.ws.service.UsersService;
import com.restapi.app.ws.service.impl.UsersServiceImpl;
import com.restapi.app.ws.shared.dto.UserDTO;
import com.restapi.app.ws.ui.model.request.CreateUserRequestModel;
import com.restapi.app.ws.ui.model.response.UserProfileRest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author robertmarinescu
 */
@Path("/users")
public class UsersEntryPoint {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
    public UserProfileRest createUser(CreateUserRequestModel requestObject){

        //Prepare UserDTO
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(requestObject, userDTO);

        //Create new User
        UsersService userService = new UsersServiceImpl();
        UserDTO createdUserProfile = userService.createUser(userDTO);

        //Prepare response

        UserProfileRest returnValue = new UserProfileRest();

        return returnValue;
    }
}
