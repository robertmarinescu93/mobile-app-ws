/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restapi.app.ws.service;

import com.restapi.app.ws.shared.dto.UserDTO;

/**
 *
 * @author robertmarinescu
 */
public interface UsersService {
    public UserDTO createUser(UserDTO user);
}
