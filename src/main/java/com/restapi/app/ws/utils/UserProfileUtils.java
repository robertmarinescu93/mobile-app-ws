/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restapi.app.ws.utils;
import com.restapi.app.ws.exceptions.MissingRequiredFieldException;
import  com.restapi.app.ws.shared.dto.UserDTO;
import com.restapi.app.ws.ui.model.response.ErrorMessages;

/**
 *
 * @author robertmarinescu
 */
public class UserProfileUtils {

    public void validateRequiredFields(UserDTO userDTO) throws MissingRequiredFieldException {

        if(userDTO.getFirstName() == null ||
                userDTO.getFirstName().isEmpty() ||
                userDTO.getLastName() == null   ||
                userDTO.getLastName().isEmpty() ||
                userDTO.getEmail() == null   ||
                userDTO.getEmail().isEmpty() ||
                userDTO.getPassword() == null   ||
                userDTO.getPassword().isEmpty()
                )
        {
                throw new MissingRequiredFieldException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
        }

    }

}
